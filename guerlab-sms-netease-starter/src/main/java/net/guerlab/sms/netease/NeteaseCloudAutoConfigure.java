package net.guerlab.sms.netease;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.guerlab.sms.server.autoconfigure.SmsConfiguration;
import net.guerlab.sms.server.loadbalancer.SmsSenderLoadBalancer;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * 网易云信发送端点自动配置
 *
 * @author guer
 */
@Configuration
@EnableConfigurationProperties(NeteaseCloudProperties.class)
@AutoConfigureAfter(SmsConfiguration.class)
public class NeteaseCloudAutoConfigure {

    /**
     * 构造网易云信发送处理
     *
     * @param properties
     *         配置对象
     * @param objectMapper
     *         objectMapper
     * @param loadbalancer
     *         负载均衡器
     * @return 网易云信发送处理
     */
    @Bean
    @Conditional(NeteaseCloudSendHandlerCondition.class)
    public NeteaseCloudSendHandler neteaseCloudSendHandler(NeteaseCloudProperties properties, ObjectMapper objectMapper,
            SmsSenderLoadBalancer loadbalancer) {
        NeteaseCloudSendHandler handler = new NeteaseCloudSendHandler(properties, objectMapper);
        loadbalancer.addTarget(handler, true);
        return handler;
    }

    public static class NeteaseCloudSendHandlerCondition implements Condition {

        @Override
        public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
            Boolean enable = context.getEnvironment().getProperty("sms.netease.enable", Boolean.class);
            return enable == null || enable;
        }
    }

}
